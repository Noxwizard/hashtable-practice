#include <stdlib.h>
#include <string.h>
#include "TestCode.h"

/**
 * Simple hashing function for ASCII strings
 * @author D. J. Bernstein
 * @copyright 1991
 * 
 * @param str Null terminated character array
 * @return hash of string
 */
unsigned long djb2_hash(unsigned char *str)
{
    unsigned long hash = 5381;
    int c;

    while (c = *str++)
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash;
}

/**
 * Initializes a hash table to the given size
 * 
 * @param size Number of entries in the hash table
 * 
 * @return Pointer to the new hash table
 */
hashtable* hashtable_init(int size)
{
    return NULL;
}

/**
 * Inserts a (key, value) pair into the hash table
 * 
 * @param table The hash table to add the entry to
 * @param key The key to store the value at
 * @param value The value to store
 * 
 * @return Success or failure of inserting the value
 */
bool hashtable_insert(hashtable* table, const char* key, const char* value)
{
    return false;
}

/**
 * Retrieves a value from the hash table
 * 
 * @param table The hash table to operate on
 * @param key The key to retrieve the value from
 * 
 * @return The value
 */
char* hashtable_get(hashtable* table, const char* key)
{
    return NULL;
}

/**
 * Frees all memory associated with the given hash table
 */
void hashtable_free(hashtable* table)
{
    return;
}
