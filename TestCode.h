#pragma once
#include <stdbool.h>

typedef struct hashtable_ {

} hashtable;

#ifdef __cplusplus
extern "C" {
#endif
    // Hash table functions
    hashtable* hashtable_init(int size);
    bool hashtable_insert(hashtable* table, const char* key, const char* value);
    char* hashtable_get(hashtable* table, const char* key);
    void hashtable_free(hashtable* table);

    // Linked list functions

#ifdef __cplusplus
}
#endif
