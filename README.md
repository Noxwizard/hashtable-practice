# Cloning
When cloning this repo, use `git clone --recurse-submodules` so that it automatically pulls the Googletest repo.
If the repo was not cloned this way, it can be updated after the fact by running `git submodule update --init`

# Objective
Implement a [hash table](https://en.wikipedia.org/wiki/Hash_table) with the following requirements:
1. The hash table's size is fixed, but specified at initialization time
   * Use `hashtable_init(size)`
   * Table must be dynamically allocated at runtime
1. Use the supplied hash function to compute the index
   * Use `djb2_hash(string)`
1. When inserting items into the hash table, the strings must be copied. The provided input cannot be assumed to be long-lived.
   * Items will be provided as `char*` keys and values
   * Use `hashtable_insert(table, key, value)`
1. Collisions should be handled using the "separate chaining" technique of using linked lists for each entry
   * Linked list code is not provided
1. When inserting a key that already exists, the value should be updated.
   * Multiple inserts using the same key should not be treated as a collision
1. Data is retrieved from the hash table using `hashtable_get(table, key)`
   * If a key does not exist in the table, return `NULL`
1. The hash table does not need to support removing entries
1. The hash table can be destroyed using `hashtable_free(table)`
   * All allocated memory must be released
1. Include error handling. For failures, return `NULL` when pointers are expected and `false` when booleans are expected.

# Building
This repo is set up for CMake and is compatible with VSCode and the CMake Tools extension if desired.

To build the project and run the tests, use the following commands:
```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
./TestCode
```

This repo uses the built-in GCC analyzers to help identify memory faults and leaks. Leak detection interferes with GDB,
so it is disabled in debug mode. To verify that there are no leaks, also build and test in release mode:
```
cd build
rm -rf *
cmake -DCMAKE_BUILD_TYPE=Release ..
make
./TestCode
```

The initial build will take a bit longer since it needs to build the googletest library first.